function filterBy(arr, dataType){
    return arr.filter(item => (typeof item !== null && typeof item !== dataType));
    
}
let inputArray = ['hello', 'world', 23, '23', null];
let filteredArray = filterBy(inputArray, 'string');

console.log('Original:', inputArray);
console.log('Filtered:', filteredArray);


// 1. For each працює як цикл, цей метод задається для виконання заданої функції один раз для кожного елемента у масиві.
// 2. Щоб очистити масив можна створит новий порожній масив та присвїти його змінну, що має ефект видалення всіх елементів попереднього масиву. 
// Встановлення довжини масиву 0 також може зробити його порожнім.
// 3. Щоб перевірити, що та чи інша змінна є масивом можна використати оператор Array.isArray і він поверне true або false.




















// function filterBy(arr, dataType) {

//     return arr.filter(item => typeof item === dataType);
// }
